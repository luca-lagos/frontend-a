/*import { useState } from 'react'
import reactLogo from './assets/react.svg'*/
import "./App.css";
import Option, { params } from "./Menu/Option";
import accept from "./images/acceptOne.png";
import styled from "styled-components";

const Container = styled.div`
  background-color: red;
`;

const App = () => {
  const options: string[] = [
    "1up nutrition",
    "asitis",
    "avvatar",
    "big muscles",
    "bpi sports",
    "bsn",
    "cellucor",
    "domin8r",
    "dymatize",
    "dymamik",
    "esn",
    "evlution nutrition",
  ];

  const handleClick: params["onClick"] = (value) => {
    console.log(value);
  };

  return (
    <Container>
      {options.map((value, index) => (
        <Option key={index} title={value} onClick={handleClick} />
      ))}
    </Container>
  );
};

/*function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}*/

export default App;
