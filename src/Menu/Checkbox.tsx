import { useEffect, useState } from "react";
import styled from "styled-components";

const Circulo = styled.div<{ visible: boolean }>`
  border-radius: 50%;
  border: 1.8px solid white;
  height: 35px;
  width: 35px;
  cursor: pointer;
  background-color: ${(props) => (props.visible ? "white" : "transparent")};
`;

const Icon = styled.img<{ visible: boolean }>`
  height: 35px;
  width: 35px;
  object-fit: contain;
  border-radius: 50%;
  visibility: ${(props) => (props.visible ? "visible" : "hidden")};
`;

export interface params {
  image: string;
  onClick?: (value: boolean) => void;
}

const App = (params: params) => {
  const [visible, setVisible] = useState(false);

  const handleClick = () => {
    setVisible(!visible);
    if (typeof params.onClick === "function") params.onClick(!visible);
  };

  return (
    <Circulo visible={visible} onClick={handleClick}>
      <Icon src={params.image} visible={visible} />
    </Circulo>
  );
};

export default App;
