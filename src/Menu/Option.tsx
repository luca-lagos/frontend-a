import styled from "styled-components";
import Checkbox from "./CheckBox";
import acceptOne from "../images/acceptOne.png";

const Container = styled.div`
  margin-top: 15px;
  margin-left: 25px;
  margin-right: 25px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const Title = styled.p`
  margin: 0px;
  color: white;
  font-weight: 500;
  text-transform: uppercase;
`;

export interface params {
  title: string;
  onClick?: (params: { value: string; state: boolean }) => void;
  /*version: number;*/
}

const App = (params: params) => {
  const handleClick = (state: boolean) => {
    if (typeof params.onClick === "function")
      params.onClick({ value: params.title, state });
  };

  /*switch (params.version) {
    case 1:
      return (
        <Container>
          <Title>{params.title}</Title>
          <Checkbox image={acceptOne} onClick={handleClick} />
        </Container>
      );
    case 2:
      return (
        <Container>
          <Title>{params.title}</Title>
          <Checkbox image={acceptOne} onClick={handleClick} />
        </Container>
      );
    default:
      return <></>;
  }*/

  return (
    <Container>
      <Title>{params.title}</Title>
      <Checkbox image={acceptOne} onClick={handleClick} />
    </Container>
  );
};

export default App;
